import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forRoot([
      { path: 'home', loadChildren: () => import('./features/home/home.module').then(m => m.HomeModule)},
      { path: 'catalog', loadChildren: () => import('./features/catalog/catalog.module').then(m => m.CatalogModule)},
      {
        path: 'contacts',
        loadChildren: () => import('./features/contacts/contacts.module').then(res => res.ContactsModule)
      },
      { path: '', redirectTo: 'home', pathMatch: 'full'},
      { path: 'settings', loadChildren: () => import('./features/settings/settings.module').then(m => m.SettingsModule) },
      { path: 'login', loadChildren: () => import('./features/login/login.module').then(m => m.LoginModule) },
      { path: 'users', loadChildren: () => import('./features/users/users.module').then(m => m.UsersModule) },
      { path: '**', redirectTo: 'home'}
    ])
  ],
  exports: [
    RouterModule,
  ]
})
export class AppRoutingModule {}
