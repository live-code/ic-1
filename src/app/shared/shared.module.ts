import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardComponent } from './components/card.component';
import { MyButtonModule } from './components/my-button.module';
import { PanelComponent } from './components/panel.component';
import { AccordionComponent } from './components/accordion.component';


@NgModule({
  exports: [
    CardComponent,
    PanelComponent,
    AccordionComponent,
    MyButtonModule
  ],
  declarations: [
    CardComponent,
    PanelComponent,
    AccordionComponent,

  ],
  imports: [
    CommonModule,
    MyButtonModule
  ]
})
export class SharedModule { }
