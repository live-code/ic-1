import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar',
  template: `
    <button routerLink="login">login</button>
    <button routerLink="home">Home</button>
    <button [routerLink]="'settings'">settings</button>
    <button [routerLink]="'catalog'">catalog</button>
    <button [routerLink]="'contacts'">contacts</button>
  `,
})
export class NavbarComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
