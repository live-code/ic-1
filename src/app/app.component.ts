import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';

type Page = 'home' | 'catalog' | 'contacts';

@Component({
  selector: 'app-root',
  template: `
    <h1>Hello IC</h1>
    <div>Currente page: {{page}}</div>
    <app-navbar></app-navbar>
    <hr>
    <router-outlet></router-outlet>
  `,
  styles: []
})
export class AppComponent {
  page: Page | null = null;

  changePage(page: Page) {
    this.page = page;
  }
}
