import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  template: `
    <p>login works!</p>
    
    <router-outlet></router-outlet>

    <hr>
    <button routerLink="signin">signin</button>
    <button routerLink="/login/lostpass">lost pass</button>
    <button routerLink="registration">registration</button>
  `,
  styles: [
  ]
})
export class LoginComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
