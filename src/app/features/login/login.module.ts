import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login.component';
import { SigninComponent } from './components/signin.component';
import { RegistrationComponent } from './components/registration.component';
import { LostpassComponent } from './components/lostpass.component';


// const routes: Routes =

@NgModule({
  declarations: [
    LoginComponent,
    SigninComponent,
    RegistrationComponent,
    LostpassComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: LoginComponent,
        children: [
          { path: 'signin', component: SigninComponent },
          { path: 'registration', component: RegistrationComponent },
          { path: 'lostpass', component: LostpassComponent },
          { path: '', redirectTo: 'signin', pathMatch: 'full'},
        ]
      }
    ])
  ]
})
export class LoginModule { }
