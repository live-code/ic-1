import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-catalog',
  template: `
    <h1>catalog works!</h1>
    <app-catalog-form></app-catalog-form>
    <app-catalog-list></app-catalog-list>
    <app-card></app-card>
  `,
  styles: [
  ]
})
export class CatalogComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
