import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { CardComponent } from '../../shared/components/card.component';
import { SharedModule } from '../../shared/shared.module';
import { CatalogComponent } from './catalog.component';
import { CatalogFormComponent } from './components/catalog-form.component';
import { CatalogListComponent } from './components/catalog-list.component';

@NgModule({
  declarations: [
    CatalogComponent,
    CatalogListComponent,
    CatalogFormComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild([
      { path: '', component: CatalogComponent}
    ])
  ]
})
export class CatalogModule { }
