import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { ContactsComponentMenu } from './contacts-menu.component';
import { ContactsComponent } from './contacts.component';
import { ContactsGuideComponent } from './components/contacts-guide.component';

@NgModule({
  declarations: [
    ContactsComponent,
    ContactsComponentMenu,
    ContactsGuideComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild([
      { path: 'help', component: ContactsGuideComponent},
      { path: '', component: ContactsComponent},
    ])
  ]
})
export class ContactsModule {}

